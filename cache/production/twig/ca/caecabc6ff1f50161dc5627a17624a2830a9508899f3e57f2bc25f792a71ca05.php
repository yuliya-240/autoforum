<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* navbar_footer.html */
class __TwigTemplate_73ce99100dafe51c7a05798ea4a23e66a89bca7a322c5726793a8f61434014a8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<nav>
\t<ul id=\"nav-footer\" class=\"nav-footer linklist\" role=\"menubar\">
\t\t<li class=\"breadcrumbs\">
\t\t\t";
        // line 4
        if (($context["U_SITE_HOME"] ?? null)) {
            // line 5
            echo "\t\t\t\t";
            ob_start(function () { return ''; });
            // line 6
            echo "\t\t\t\t<span class=\"crumb\">
\t\t\t\t\t<a href=\"";
            // line 7
            echo ($context["U_SITE_HOME"] ?? null);
            echo "\" data-navbar-reference=\"home\">
\t\t\t\t\t\t<span>";
            // line 8
            echo $this->extensions['phpbb\template\twig\extension']->lang("SITE_HOME");
            echo "</span>
\t\t\t\t\t</a>
\t\t\t\t</span>
\t\t\t\t";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 12
            echo "\t\t\t";
        }
        // line 13
        echo "\t\t\t";
        // line 14
        echo "\t\t\t";
        ob_start(function () { return ''; });
        // line 15
        echo "\t\t\t<span class=\"crumb\">
\t\t\t\t<a href=\"";
        // line 16
        echo ($context["U_INDEX"] ?? null);
        echo "\" data-navbar-reference=\"index\">
\t\t\t\t\t<span>";
        // line 17
        echo $this->extensions['phpbb\template\twig\extension']->lang("INDEX");
        echo "</span>
\t\t\t\t</a>
\t\t\t</span>
\t\t\t";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 21
        echo "\t\t\t";
        // line 22
        echo "\t\t</li>

\t\t";
        // line 24
        if (($context["U_ACP"] ?? null)) {
            // line 25
            echo "\t\t\t<li class=\"small-icon\" data-last-responsive=\"true\">
\t\t\t\t<a href=\"";
            // line 26
            echo ($context["U_ACP"] ?? null);
            echo "\" title=\"";
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP");
            echo "\" role=\"menuitem\">";
            echo $this->extensions['phpbb\template\twig\extension']->lang("ACP_SHORT");
            echo "</a>
\t\t\t</li>
\t\t";
        }
        // line 29
        echo "\t\t";
        if ((($context["U_CONTACT_US"] ?? null) &&  !($context["S_IS_BOT"] ?? null))) {
            // line 30
            echo "\t\t\t<li class=\"small-icon\" data-last-responsive=\"true\">
\t\t\t\t<a href=\"";
            // line 31
            echo ($context["U_CONTACT_US"] ?? null);
            echo "\" role=\"menuitem\">";
            echo $this->extensions['phpbb\template\twig\extension']->lang("CONTACT_US");
            echo "</a>
\t\t\t</li>
\t\t";
        }
        // line 34
        echo "
\t\t";
        // line 35
        // line 36
        echo "\t\t";
        // line 37
        echo "
\t\t";
        // line 38
        if ( !($context["S_IS_BOT"] ?? null)) {
            // line 39
            echo "\t\t\t<li class=\"small-icon\">
\t\t\t\t<a href=\"";
            // line 40
            echo ($context["U_DELETE_COOKIES"] ?? null);
            echo "\" data-ajax=\"true\" data-refresh=\"true\" role=\"menuitem\">";
            echo $this->extensions['phpbb\template\twig\extension']->lang("DELETE_COOKIES");
            echo "</a>
\t\t\t</li>
\t\t";
        }
        // line 43
        echo "
\t\t";
        // line 44
        // line 45
        echo "\t\t<li>";
        echo ($context["S_TIMEZONE"] ?? null);
        echo "</li>
\t\t";
        // line 46
        // line 47
        echo "\t</ul>
</nav>
";
    }

    public function getTemplateName()
    {
        return "navbar_footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 47,  149 => 46,  144 => 45,  143 => 44,  140 => 43,  132 => 40,  129 => 39,  127 => 38,  124 => 37,  122 => 36,  121 => 35,  118 => 34,  110 => 31,  107 => 30,  104 => 29,  94 => 26,  91 => 25,  89 => 24,  85 => 22,  83 => 21,  76 => 17,  72 => 16,  69 => 15,  66 => 14,  64 => 13,  61 => 12,  54 => 8,  50 => 7,  47 => 6,  44 => 5,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "navbar_footer.html", "");
    }
}
