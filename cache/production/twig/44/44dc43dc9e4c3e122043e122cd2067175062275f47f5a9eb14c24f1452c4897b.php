<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @phpbb_ads/includes/ad_blocker.html */
class __TwigTemplate_043a3d5d5cc52f66eb906c0afe97ad6412ef36550afe8a4c9f758b9ec63ea639 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["S_DISPLAY_ADBLOCKER"] ?? null)) {
            // line 2
            echo "\t<div id=\"phpbb-ads-ab\" class=\"rules\" style=\"display: none;\">
\t\t<div class=\"inner\">
\t\t\t<strong>";
            // line 4
            echo ($this->extensions['phpbb\template\twig\extension']->lang("ADBLOCKER_TITLE") . $this->extensions['phpbb\template\twig\extension']->lang("COLON"));
            echo "</strong> ";
            echo $this->extensions['phpbb\template\twig\extension']->lang("ADBLOCKER_MESSAGE");
            echo "
\t\t</div>
\t</div>

\t<script>
\t\t'use strict';

\t\t// Test presence of AdBlock and show message if present
\t\t// Credit: https://christianheilmann.com/2015/12/25/detecting-adblock-without-an-extra-http-overhead/
\t\tvar test = document.createElement('div');
\t\ttest.innerHTML = '&nbsp;';
\t\ttest.className = 'adsbox';
\t\tdocument.body.appendChild(test);
\t\twindow.setTimeout(function() {
\t\t\tif (test.offsetHeight === 0) {
\t\t\t\tdocument.getElementById('phpbb-ads-ab').removeAttribute('style');
\t\t\t}
\t\t\ttest.remove();
\t\t}, 100);
\t</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@phpbb_ads/includes/ad_blocker.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@phpbb_ads/includes/ad_blocker.html", "");
    }
}
